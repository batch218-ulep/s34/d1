
// this code will help us access contents of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
	// It also allows us to access methods and functions that we will use to easily create an app/server
	// we store our express module to variable so we could easily access its keywords, functions, and methods.
const express = require("express");
	
	// This code creates an application using express / a.k.a express application
	// App is our server
const app = express();

const port = 3000;

//Middleware
// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applyhing the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//[SECTION] Routes
//It holds our HTTP Methods
//Endpoints "/login" , "/posts", "/todos" , "/"

//GET Method

//EXPRESS
app.get("/", (req, res) => {
	res.send("Hello World!");
})

app.get("/hello", (req, res) => {
	res.send("GET method success. \nHello from /Hello endpont!");
})
// This is the response that we will expect to receive if the get method with the right endpoint is successful

//create a new endpoint with you first name
//res.send("Hello I am "fullName");

//POST Method

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		console.log(users);
		res.send(`User ${req.body.username} successfully registered!`);
	}else{
		res.send("Please input BOTH username and password");
	}

})

//PUT Method

app.put("/change-password", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password
			message = `User ${req.body.username}'s password has been updated.`
			break;
		}else{
			message = "User does not exist."
		}
	}

	res.send(message);
	console.log(users);
})

//Activity
//#1
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage")
});

//#2
app.get("/users", (req, res) => {
	res.send(users);
})

//#3
app.delete("/delete-user", (req, res) =>{
	let message;

	if(users.length != 0){
		for(let i = 0; i < users.length; i++){
			if(req.body.username == users[i].username){
				users.splice(i, 1);
				message = `User ${req.body.username} has been deleted`;
				break;
			}else{
				message = "User does not exist.";
			}
		}
	}

	res.send(message);
})


// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server is running at port ${port}`));
